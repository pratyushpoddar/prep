(function() {
  // alert('hello');

  var positionGlobal = {
    coords: {
      latitude: convertDecDeg(0),
      longitude: convertDecDeg(0)
    }
  };

  function show(notification) {

  };

  function convertDecDeg(v,tipo) {
    if (!tipo) tipo='N';
    var deg;
    deg = v;
    if (!deg){
      return "";
    } else if (deg > 180 || deg < 0){

      return convertDecDeg(-v,(tipo=='N'?'S': (tipo=='E'?'W':tipo) ));
    } else {
      var gpsdeg = parseInt(deg);
      var remainder = deg - (gpsdeg * 1.0);
      var gpsmin = remainder * 60.0;
      var D = gpsdeg;
      var M = parseInt(gpsmin);
      var remainder2 = gpsmin - (parseInt(gpsmin)*1.0);
      var S = parseInt(remainder2*60.0);
      return D+"° "+M+"' "+S+"'' "+tipo;
    }
  }

  function notify(notification, title) {

    navigator.vibrate(200);
    cordova.plugins.notification.local.schedule({
      id: 1,
      title: title || 'Bachao!',
      text: notification
    });
  }

  function onSuccessLocation(position) {
    positionGlobal.coords.latitude = convertDecDeg(position.coords.latitude);
    positionGlobal.coords.longitude = convertDecDeg(position.coords.longitude);
  };

  function onFinalErrorLocation(error) {

  }

  function onErrorLocation(error) {
    navigator.geolocation.getCurrentPosition(onSuccessLocation, onFinalErrorLocation, { timeout: 60000, enableHighAccuracy: false });
  }

  function startLocationWatch() {
    navigator.geolocation.watchPosition(onSuccessLocation,onFinalErrorLocation, { enableHighAccuracy: true });
  }

  function getLocation(callback) {
    navigator.geolocation.getCurrentPosition(onSuccessLocation, onErrorLocation, { timeout: 60000, enableHighAccuracy: true });
    callback({
      lat: positionGlobal.coords.latitude,
      lon: positionGlobal.coords.longitude
    });
  }


  function sendSMS(number, message, success, failure) {
    var success = success || 'Message sent successfully';
    var failure = failure || 'Sorry, message could not be sent';

    // process the confirmation dialog result
    function onConfirm(buttonIndex) {
      if (buttonIndex == 1) {
        if(SMS) SMS.sendSMS(number, message, function(){
          notify(success);
        }, function(){
          notify(failure);
        });
      }
    }

    function showConfirmSMS() {
      navigator.notification.confirm(
            'Are you sure you want to notify your emergency contacts?', // message
             onConfirm,            // callback to invoke with index of button pressed
            'Confirm SMS sending',           // title
            ['Ok', 'Cancel']         // buttonLabels
            );
    }

    showConfirmSMS();

  };

  function attachPhoneListEvents() {
    $('#contact-list').off('click').on('click', 'li', function(e) {
      var name = $(this).text().split(',')[0];
      var phone = $(this).text().split(',')[1];

      var contacts = [];
      if (localStorage.getItem('contacts'))
        var arr = JSON.parse(localStorage.getItem('contacts'));
      if (arr && arr.length > 0) {
        contacts = arr;
      }
      else {
        localStorage.setItem('contacts', contacts);
      }

      if (contacts.length <= 5) {
        contacts.push({
          'id': contacts.length,
          'name': name,
          'phone': phone.replace(/-|\s/g,"")
        });
        console.log(contacts);
        $('.ui-input-search input').val('').trigger('change');

        localStorage.setItem('contacts', JSON.stringify(contacts));
        $.mobile.changePage( "#add-contacts", { transition: "none", changeHash: false });
      }
      else {
        alert('cannot add anymore');
        $.mobile.changePage( "#add-contacts", { transition: "none", changeHash: false });
      }

    });
  };

  function populateList(contacts) {
    var contactList = '';
    contacts.forEach(function(contact) {
      if (contact.phoneNumbers) {
        contact.phoneNumbers.forEach(function(phoneNumber) {
          contactList += '<li>' + contact.displayName + ', ' + phoneNumber["value"] + '</li>';
        });
      }
    });
    $('#contact-list').html(contactList).listview('refresh');
    attachPhoneListEvents();
  };

  function fetchContacts() {
    var options      = new ContactFindOptions();
    options.filter   = "";
    options.multiple = true;
    // options.desiredFields = [navigator.contacts.fieldType.phoneNumbers, navigator.contacts.fieldType.name, navigator.contacts.fieldType.displayName];
    var fields       = [navigator.contacts.fieldType.displayName, navigator.contacts.fieldType.name];

    navigator.contacts.find(fields, function(contacts) {
      populateList(contacts);
    } , function(error) {
      notify('Contacts could not be fetched');
    }, options);
  }

  function fetchPhoneNumbers() {
    var contacts = [];
    if (localStorage.getItem('contacts'))
      contacts = JSON.parse(localStorage.getItem('contacts'));

    var phoneNumbers = [];
    contacts.forEach(function(contact) {
      phoneNumbers.push(contact.phone);
    });
    return (phoneNumbers);
  }

  startLocationWatch();

  $(document).on('pageshow', '#register', function() {

    if (localStorage.getItem('phone') && localStorage.getItem('name'))
      return $.mobile.changePage( "#home", { transition: "none", changeHash: false });

    $('#register-phone').on('submit', function(e) {
      e.preventDefault();
      localStorage.setItem('phone', $('#mobile').val());
      localStorage.setItem('name', $('#name').val());
      $.mobile.changePage( "#add-contacts", { transition: "none", changeHash: false });
    });
  });

  $(document).on('pageshow', '#notify-people', function() {
    // var level = 100;
    // my_media.setVolume(level);
    $('#siren').trigger('play');
    $('#siren').loop= true;

  });
  $(document).on('pagehide', '#notify-people', function() {
    // var level = 0;
    // my_media.setVolume(level);
    $('#siren').trigger('pause');

  });
  $(document).on('pageshow', '#add-contacts', function() {
    // localStorage.setItem('contacts', []); // to clear the contact list
    var contacts = [];
    function fetchList() {
      if (localStorage.getItem('contacts'))
        contacts = JSON.parse(localStorage.getItem('contacts'));

      if (contacts.length < 5) $('.add-contact-button').show();
      else $('.add-contact-button').hide();

      if (contacts.length == 0) $('.proceed-to-home').hide();
      else $('.proceed-to-home').show();

      var contactList = '';
      contacts.forEach(function(contact) {
        contactList += '<li data-icon="delete" data-id="' + contact['id'] + '" ><a href=""><h2>' + contact['name'] + '</h2><p>' + contact['phone'] + '</p></a><a href="#">Remove</a></li>';
      });

      $('#emergency-contacts').html(contactList).listview('refresh');
    };

    fetchList();

    $('#emergency-contacts').off('click').on('click', 'li .ui-icon-delete', function() {
      var id = $(this).parents('li').data('id');

      var newContacts = [], count = 0;
      for (var i = 0; i < contacts.length; i++) {
        if (contacts[i]['id'] !== id)
          newContacts[count++] = contacts[i];
      };

      localStorage.setItem('contacts', JSON.stringify(newContacts));
      fetchList();
      // $.mobile.changePage( "#add-contacts", { transition: "none", changeHash: false });
    });
  });

  $(document).on('pageinit', '#pickContacts', function() {
    // notify('hello');
    // localStorage.setItem('contacts', []);
    fetchContacts();
  });

  $(document).on('pageshow', '#home', function() {

    var contacts = [];
    if (localStorage.getItem('contacts'))
      contacts = JSON.parse(localStorage.getItem('contacts'));
    if (contacts.length <= 0)
      return $.mobile.changePage( "#add-contacts", { transition: "none", changeHash: false });

    $( '#checkin-popup' ).popup({
      afteropen: function( event, ui ) {
        var message = 'Find ' + localStorage.getItem('name') + ' at ' + positionGlobal.coords.latitude + ' ' + positionGlobal.coords.longitude ;
        $('#casual-checkin-content').val(message);
      }
    });

    $('#checkin-form').off('submit').on('submit', function(e) {
      e.preventDefault();
      var phoneNumbers = fetchPhoneNumbers();
      // alert(message);
      var message = $('#casual-checkin-content').val();
      // alert(message);
      sendSMS(phoneNumbers, message, 'Your check in information is sent to all your emergency contacts.');
      return $.mobile.changePage( "#home", { transition: "none", changeHash: false });
    });

    $('.safe-mark').off('click').on('click', function() {
      var phoneNumbers = fetchPhoneNumbers();
      var message = 'Find ' + localStorage.getItem('name') + ' safe at ' + positionGlobal.coords.latitude + ' ' + positionGlobal.coords.longitude;
      // alert(message);
      sendSMS(phoneNumbers, message, 'Your location is sent to your emergency contacts and you are marked as safe. Help others around you!');
    });

    $('.danger-trigger').off('click').on('click', function() {

      var phoneNumbers = fetchPhoneNumbers();
      var message = 'Find ' + localStorage.getItem('name') + ' in danger at ' + positionGlobal.coords.latitude + ' ' + positionGlobal.coords.longitude;
      // alert(message);
      sendSMS(phoneNumbers, message, 'Your location is sent to your emergency contacts. While they provide some help, have a look at Interactive Tutorials OR use Shout!.', 'Message sending failed, please try again');
    });

  });

})();















var app = {
  initialize: function() {
    this.bindEvents();
  },
  bindEvents: function() {
    document.addEventListener('deviceready', this.onDeviceReady, false);
  },

  onDeviceReady: function() {
    app.receivedEvent('deviceready');
  },

  receivedEvent: function(id) {
    var parentElement = document.getElementById(id);
    var listeningElement = parentElement.querySelector('.listening');
    var receivedElement = parentElement.querySelector('.received');

    listeningElement.setAttribute('style', 'display:none;');
    receivedElement.setAttribute('style', 'display:block;');

    console.log('Received Event: ' + id);

    function alertDismissed() {

    }

    function show(notification) {
      document.getElementById('wrick').innerHTML = notification;
      navigator.vibrate(200);
    };

    function notify(notification) {

      cordova.plugins.notification.local.schedule({
        id: 1,
        text: notification
      });
    }

    function onSuccessLocation(position) {
      document.getElementById('message').innerHTML = "Got it";
      show('Latitude: '          + position.coords.latitude          + '<br/>' +
        'Longitude: '         + position.coords.longitude         + '<br/>' +
        'Altitude: '          + position.coords.altitude          + '<br/>');
      show(notification);
    };

    function onFinalErrorLocation(error) {
      document.getElementById('message').innerHTML = "Reload the app";
      show('code: '    + error.code    + '\n' +
        'message: ' + error.message + '\n');
    }

    function onError(error) {
      navigator.geolocation.getCurrentPosition(onSuccessLocation, onFinalErrorLocation, { timeout: 60000, enableHighAccuracy: false });
      show('Retrying');
    }

    show('Getting your location');

    // localStorage.setItem('name', 'wrick');
    // notify(localStorage.getItem('name'));

    show(JSON.stringify(navigator.contacts));

    function onSuccessContact(contacts) {
      var str = '<h4>Contacts:</h4>';
      contacts.forEach(function(contact) {
        str += '<br/>' + contact.displayName;
      });
      show(str);
    };

    function onErrorContact(contactError) {
      alert('onError!');
    };

    function sendSms(number, message) {
      if(SMS) SMS.sendSMS(number, message, function(){
        notify('SMS sent');
      }, function(){
        notify('SMS sending failed');
      });
    };

    document.getElementById('sms').addEventListener('click', function() {
      sendSms('+919432909925', 'Hackathon Test Message');
    }, false);

    // find all contacts with 'Bob' in any name field
    var options      = new ContactFindOptions();
    options.filter   = "";
    options.multiple = true;
    // options.desiredFields = [navigator.contacts.fieldType.id];
    var fields       = [navigator.contacts.fieldType.displayName, navigator.contacts.fieldType.name];
    navigator.contacts.find(fields, onSuccessContact, onErrorContact, options);

    // navigator.geolocation.getCurrentPosition(onSuccess, onError, { timeout: 60000, enableHighAccuracy: true });
  }
};